<?php
use PHPUnit\Framework\TestCase;
use BLFrameWork\User;

class UserTest extends TestCase{
    private $userClass;
    protected  function setUp() : void{
        $this->userClass = new User();
    }

    //Vérification du système d'attribut d'utilisateur

    /**
     * @dataProvider providerForAttr
     */
    public function testAttr($a,$b,$expected){
        $this->userClass->setAttribute($a,$b);
        $result = $this->userClass->getAttribute($a);
        $this->assertSame($expected,$result);
        unset($_SESSION[$a]);
    }
    public function providerForAttr(){
        return [
                'AddOne' => ['t','bien','bien'],
                'noAdd' => ['test','',''],
        ];
    }
    //Vérification du système de 'flash'

    /**
     * @dataProvider providerForFlash
     */
    public function testFlash($a){
        $this->userClass->setFlash($a);
        $this->assertSame(true,$this->userClass->hasFlash());
        $this->assertSame($a,$this->userClass->getFlash($a));
        $this->assertSame(false,$this->userClass->hasFlash());
    }

    public function providerForFlash(){
        return [
            'testOne' => ['test',true],
            'testTwo' => [0,true]
        ];
    }

    //Vérification du système d'authentification

    /**
     * @dataProvider providerForAuth
     */
    public function testAuth($a,$b,$c){
        if($c != null){
            $this->expectException($c);
        }
        $this->userClass->setAuthenticated($a);
        $this->assertSame($b,$this->userClass->isAuthenticated());
    }
    public function providerForAuth(){
        return [
            'testOne' => [true,true,null],
            'testTwo' => ['test',false,'InvalidArgumentException'],
            'testThree' => [false,false,null],
            'testFour' => [0,false,'InvalidArgumentException']
        ];
    }
}
