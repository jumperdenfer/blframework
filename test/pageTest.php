<?php
use PHPUnit\Framework\TestCase;
use BLFrameWork\Page;
use BLFrameWork\Application;

class pageTest extends TestCase{
    private $applicationTest;
    private $classTest;
    protected  function setUp() : void{
        $this->applicationTest = new class extends Application{
            public function __construct(){
                parent::__construct();
                $this->name = 'Frontend';
            }

            public function run(){

                $controller = $this->getController();
                $controller->execute();

                $this->httpResponse->setPage($controller->page());
                $this->httpResponse->send();
            }
        };
        $this->classTest = new Page($this->applicationTest);
    }

    //Test de l'ajout des variable

    /**
    * @dataProvider providerForVar
    */
    public function testAddVar($a,$b,$error){
        if($error != null){
            $this->expectException($error);
        }
        $this->classTest->addVar($a,$b);
        $this->assertSame($b,$this->classTest->existVar($a));
    }
    public function providerForVar(){
        return [
            'testOne' => ['a','a',null],
            "testTwo" => [0,'a','InvalidArgumentException'],
            "testThree" => ['test','0',null]
        ];
    }

    //Test du layout

    /**
    * @dataProvider providerForLayout
    */
    public function testLayout($a,$b){
        $this->classTest->setLayout($a);
        $this->assertSame($b,$this->classTest->getLayoutName());
    }
    public function providerForLayout(){
        return [
            'testOne'=> ['test 1', 'test 1'],
            'testThree' => [10,10]
        ];
    }


    //Test du setContentFile et getGeneratedPage

    /**
    * @dataProvider providerForSetContentFile
    */
    public function testSetContentFile($a,$b,$c){
        if($b !=null){
            $this->expectException($b);
        }
        $this->classTest->setContentFile($a);
        $this->expectException('\RuntimeException');

        $this->classTest->getGeneratedPage();

    }
    public function providerForSetContentFile(){
        return [
            'testOne' => [__DIR__."/testContentFile.php",null,'test'],
            'testTwo' => ['','\InvalidArgumentException',null]
        ];
    }
}
