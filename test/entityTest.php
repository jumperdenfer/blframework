<?php
use PHPUnit\Framework\TestCase;
use BLFrameWork\Entity;

class EntityTest extends TestCase{
    private $classTest;
    protected  function setUp() : void{
        $this->classTest = new Entity();
    }
    //Test isNew
    public function testIsNew(){
        $this->assertSame(true,$this->classTest->isNew());
    }
    /**
     * @dataProvider providerSetId
     */
    public function testSetId($a,$expected){
        $this->classTest->setId($a);
        $this->assertSame($expected,$this->classTest->offsetGet('id'));
    }
    public function providerSetId(){
        $numberTesting = rand();
        return [
            'setWithNumber' => [$numberTesting,$numberTesting],
            'setWithChars' => ['a','a'],
            'setWithWord' => ['test','test']
        ];
    }
    //Test offsetExist
    public function testOffsetExist(){
        $this->assertSame(false,$this->classTest->offsetExists('id'));
    }
}
