<?php
use PHPUnit\Framework\TestCase;
use BLFrameWork\http\HttpRequest;
use BLFrameWork\Application;
class HttpRequestTest extends TestCase{
    private $requestClass;
    private $applicationTest;
    protected function setUp() : void{
        $this->applicationTest = new class extends Application{
            public function __construct(){
                parent::__construct();
                $this->name = 'Frontend';
            }

            public function run(){

                $controller = $this->getController();
                $controller->execute();

                $this->httpResponse->setPage($controller->page());
                $this->httpResponse->send();
            }
        };
        $this->requestClass = new HttpRequest($this->applicationTest);

    }

    //Test les méthode lié aux cookie

    /**
    * @dataProvider providerForCookie
    */
    public function testCookie($a,$b,$c){
        $_COOKIE[$a] = $b;
        $this->assertSame($c,$this->requestClass->cookieExists($a));
        $this->assertSame($b,$this->requestClass->cookieData($a));
        unset($_COOKIE[$a]);
    }
    public function providerForCookie(){
        return [
            'TestOne' => ['Cookie','Miam',true],
            'TestTwo' => ['Cake','Is a lie',true],
            'TestThre' => [true,false,true],
            'TestFour' => [1,5,true]
        ];
    }

    //Test les méthode lié au GET

    /**
    * @dataProvider providerForGet
    */
    public function testGetData($a,$b,$c){
        if($b != null){
            $_GET[$a] = $b;
        }
        $this->assertSame($c,$this->requestClass->getExists($a));
        $this->assertSame($b,$this->requestClass->getData($a));
        unset($_GET[$a]);
    }
    public function providerForGet(){
        $numberRand = rand();
        return [
            'TestOne' => ['getget','il guette',true],
            'TestTwo' => [$numberRand,$numberRand,true],
            'TestThree' => ['test',null,false]
        ];
    }
    //Test les méthode lié au POST

    /**
    * @dataProvider providerForPost
    */
    public function testPostData($a,$b,$c,$d){
        $_POST[$a] = $b;
        $this->assertSame($d,$this->requestClass->postExists($a));
        $this->assertSame($c,$this->requestClass->postData($a));

        unset($_POST[$a]);
    }
    public function providerForPost(){
        $nbrRand = rand();
        return [
            'TestOne' => ['testtest', 'testeur','testeur',true],
            'TestTwo' => [$nbrRand,$nbrRand,$nbrRand,true]
        ];
    }
}
