<?php
use PHPUnit\Framework\TestCase;
use BLFrameWork\Form\Fields\MailField;
use BLFrameWork\Form\ValidatorFactory;
class MailFieldTest extends TestCase{
    /**
     * @dataProvider providerForConstruct
     */
    public function testConstruct($data,$reponse){
        //Test create
        $classTest = new MailField($data);
        $this->assertSame($reponse[0],$classTest->label());
        $this->assertSame($reponse[1],$classTest->name());
        $this->assertSame($reponse[2],$classTest->value());
        //Test buildWidget
        $widget = $classTest->buildWidget();
        $labelTest = $reponse[0];
        $nameTest = $reponse[1];
        $valueTest = $reponse[2];
        $testWidget = "<label>{$labelTest}</label><input type='mail' name='{$nameTest}' value='{$valueTest}' >";
        $this->assertSame($testWidget,$widget);
        $this->assertSame($reponse[3],$classTest->isValid());
    }
    public function providerForConstruct(){
        return [
            'testOne' => [
                [
                    'label' => 'test',
                    'name' => 'super',
                    'value' => 'mega@test.fr',
                    "validator" =>[
                        ValidatorFactory::generateValidator('BLFrameWork','NotNullValidator',"Ce champs ne peut pas être null")
                    ]
                ],
                [
                    'test','super','mega@test.fr',true
                ]
            ],
            'testTwo' =>[
                [
                    'label' => 'P0',
                    'name' => 'P1',
                    "value" => '2@test.fr',
                    "validator" => [
                        ValidatorFactory::generateValidator('BLFrameWork',"NotNullValidator",'Ce champs doit contenir au moins 20 caractère',5)
                    ]
                ],
                [
                    'P0','P1','2@test.fr',true
                ]
            ]
        ];
    }


}
