<?php
namespace BLFrameWork;
use BLFrameWork\Entity;
abstract class Manager{
    /**
    * @var string
    */
    protected $dao;
    /**
    * @param string $dao
    */
    public function __construct($dao){
        $this->dao = $dao;
    }
    /**
    * @param Entity $entity
    * @return void
    */
    abstract public function save($entity);
}
