<?php
namespace BLFrameWork;

abstract class ApplicationComponent{
    /**
    * @var Application
    */
    protected $app;

    public function __construct(Application $app){
        $this->app = $app;
    }
    /**
    * @return Application
    */
    public function app(){
        return $this->app;
    }
}
