<?php
namespace BLFrameWork\Traits;

trait Hydrator{
    /**
    * @param array<string|int|float|object> $data
    * @return void
    */
    public function hydrate(array $data){
        foreach($data as $key => $value){
            $method = 'set'.ucFirst($key);
            if(is_callable([$this,$method])){
                $this->$method($value);
            }
        }
    }
}
