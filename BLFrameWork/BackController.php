<?php
namespace BLFrameWork;
use BLFrameWork\ApplicationComponent;
use BLFrameWork\Managers;

//Génère un code d'erreur du au fait, qu'a cette emplacement, l'autoloader n'est pas chargé
abstract class BackController extends ApplicationComponent{
    /**
    * @var string
    */
    protected $action = '';
    /**
    * @var string
    */
    protected $module = '';
    /**
    * @var Page
    */
    protected $page;
    /**
    * @var string
    */
    protected $view = '';
    /**
    * @var Managers
    */
    protected $managers;

    public function __construct(Application $app,string $module, string $action){
        parent::__construct($app);

        $this->managers  = new Managers($app);

        $this->page = new Page($app);
        $this->setModule($module);
        $this->setAction($action);
        //By default use a view that has the same name of the action;
        $this->setView($action);

    }
    /**
    * @return void
    */
    public function execute(){
        $method = 'execute'.ucFirst($this->action);
        if(!is_callable([$this,$method])){
            throw new \RuntimeException('L\'Action "'.$this->action.'" n\'est pas définie sur ce module');
        }
        $this->$method($this->app->httpRequest());
    }
    /**
    * @return Page
    */
    public function page(){
        return $this->page;
    }
    /**
    * @param string $module
    * @return void
    */
    public function setModule($module){
        if(!is_string($module) || empty($module)){
            throw new \InvalidArgumentException('Le module doit être une chaine de charactères valide.');
        }
        $this->module = $module;
    }
    /**
    * @param string $action
    * @return void
    */
    public function setAction($action){
        if(!is_string($action) || empty($action)){
            throw new \InvalidArgumentException('L\'action doit être une chaine de charactères valide.');
        }
        $this->action = $action;
    }
    /**
    * @param string $view
    * @return void
    */
    public function setView($view){
        if(!is_string($view) || empty($view)){
            throw new \InvalidArgumentException('la vue doit être une chaine de charactères valide.');
        }
        $this->view = $view;
        $vendorExit = \str_replace("vendor\blagrange\blframework\BLFrameWork","",__DIR__);
        $this->page->setContentFile($vendorExit.'/Resources/Views/'.$this->view.'.php');
    }

}
