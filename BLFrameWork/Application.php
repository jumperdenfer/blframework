<?php
namespace BLFrameWork;

use BLFrameWork\Http\HttpRequest;
use BLFrameWork\Http\HttpResponse;
use BLFrameWork\Router\Router;
use BLFrameWork\Router\Route;
use BLFrameWork\User;
use BLFrameWork\Config;

abstract class Application{
    /**
    * @var HttpRequest
    */
    protected $httpRequest;
    /**
    * @var HttpResponse
    */
    protected $httpResponse;
    /**
    * @var string
    */
    protected $name;
    /**
    * @var Config
    */
    public $config;
    /**
    * @var User
    */
    public $user;

    public function __construct(){
        $this->httpRequest = new HttpRequest($this);
        $this->httpResponse = new HttpResponse($this);
        $this->name = '';
        $this->user = new User();
        $this->config = new Config($this);
    }
    /**
    * @return object
    */
    public function getController(){
        $router = new Router();
        $vendorExit = \str_replace("vendor\blagrange\blframework\BLFrameWork","",__DIR__);
        $json = file_get_contents($vendorExit.'/App/'.$this->name.'/config/route.json');
        if($json == false){
            throw new \RuntimeException("Error config : File not found");
        }
        $routes = json_decode($json);
        //Ajout d'un message d'erreur dans le cas ou l'utilisateur à mal écrit le JSON de route
        if(\json_last_error() != 0){
            throw new \RuntimeException("Error JSON config:".\json_last_error_msg());
        }
        foreach ($routes as $route) {
            $vars = [];
            $middleware = [];
            if($route->vars ?? null){
                $vars = explode(',',$route->vars);
            }
            if($route->middleware ?? null){
                $middleware = explode(',',$route->middleware);
            }
            $router->addRoute(new Route($route->url,$route->module,$route->action,$vars,$middleware));
        }
        try{

            $matchedRoute = $router->getRoute($this->httpRequest->requestURI());
            $_GET = array_merge($_GET,$matchedRoute->vars());
            //Temporairement écrit ici, reflexion sur l'emplacement exacte à déterminé
            if($matchedRoute->hasMiddleware()){
                $middlewareList = $matchedRoute->middleware();
                $middleware = [];
                foreach($middlewareList as $key){
                    $middlewareClass = "App\\".$this->name.'\\Middleware\\'.$key.'Middleware';
                    $middlewareClass::run($this);
                }
            }
            $controllerClass = "App\\".$this->name.'\\Modules\\'.$matchedRoute->module().'\\'.$matchedRoute->module().'Controller';
            return new $controllerClass($this,$matchedRoute->module(),$matchedRoute->action());

        }catch(\RuntimeException $e){
            if ($e->getCode() == Router::NO_ROUTE){
                $this->httpResponse->redirect404();
                exit();
            }
        }
        throw new \RuntimeException("Erreur durant le processus d'initialisations");
    }

    /**
    * @return void
    */
    abstract public function run();
    /**
    * @return HttpRequest
    */
    public function httpRequest(){
        return $this->httpRequest;
    }
    /**
    * @return HttpResponse
    */
    public function httpResponse(){
        return $this->httpResponse;
    }
    /**
    * @return string
    */
    public function name(){
        return $this->name;
    }
}
