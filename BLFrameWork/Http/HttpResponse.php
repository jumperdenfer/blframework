<?php
namespace BLFrameWork\Http;

use BLFrameWork\ApplicationComponent;
use BLFrameWork\Page;

class HttpResponse extends ApplicationComponent{
    /**
    * @var Page
    */
    private $page;
    /**
    * addHeader
    * Définie un paramètre Header
    * @param string $header
    * @return void
    */
    public function addHeader($header){
        header($header);
    }
    /**
    * redirect
    * Définie une redirection ( hors 404)
    * @param string $location
    * @return void
    */
    public function redirect($location){
        header('Location: '.$location);
        exit();
    }
    /**
    * redirect4040
    * Définie la redirection 404
    * @return void
    */
    public function redirect404(){
        $this->page = new Page($this->app);
        $vendorExit = \str_replace("vendor\blagrange\blframework\BLFrameWork","",__DIR__);
        $this->page->setContentFile($vendorExit.'/Error/404.html');
        $this->addHeader('HTTP/1.0 404 Not Found');
        $this->send();
    }
    /**
    * send
    * Execute la génération de page
    * @return void
    */
    public function send(){
        exit($this->page->getGeneratedPage());
    }
    /**
    * setCookie
    * @param string $name
    * @param string $value
    * @param int $expire
    * @param string $path
    * @param string $domain
    * @param boolean $secure
    * @param boolean $httpOnly
    * @return void
    */
    public function setCookie($name,$value ='',$expire = 0,$path = '',$domain = '',$secure=false,$httpOnly = true){
        setCookie($name,$value,$expire,$path,$domain,$secure,$httpOnly);
    }
    /**
    * setPage
    * Définie la page
    * @param page $page
    * @return void
    */
    public function setPage(Page $page){
        $this->page = $page;
    }
}
