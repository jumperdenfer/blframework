<?php
namespace BLFrameWork\Http;

use BLFrameWork\ApplicationComponent;

class HttpRequest extends ApplicationComponent{
    /**
    * cookieData
    *
    * Vérifie l'existence d'un cookie et le return
    * @param string $key Identifier
    * @return string|null
    */
    public function cookieData($key){
        return $_COOKIE[$key] ?? null;
    }
    /**
    * cookieExists
    * Vérifie l'existence d'un cookie et renvoie un boolean
    * @param string $key Identifier
    * @return boolean
    */
    public function cookieExists($key){
        return isset($_COOKIE[$key]);
    }
    /**
    * getData
    * Récupère une valeur GET
    * @param string $key Identifier
    * @return string|null
    */
    public function getData($key)
    {
        return $_GET[$key] ?? null;
    }
    /**
    * getExists
    * Vérifie l'existence d'un paramètre GET
    * @param string $key Identifier
    * @return boolean
    */
    public function getExists($key)
    {
        return isset($_GET[$key]);
    }

    /**
    * method
    * Renvoie la method utiliser pour la requête
    * @return string|null;
    */
    public function method(){
        return $_SERVER['REQUEST_METHOD'];
    }
    /**
    * postData
    * Vérifie l'existence d'une données envoyé en post et la renvoie
    * @param string $key Identifier
    * @return string|null
    */
    public function postData($key){
        return $_POST[$key] ?? null;
    }
    /**
    * postExists
    * Vérifie l'existence d'une données envoyé en post
    * @param string $key Identifier
    * @return boolean
    */
    public function postExists($key){
        return isset($_POST[$key]);
    }
    /**
    * filesExists
    * Vérifie l'existence d'une données de type file
    * @param string $key
    * @return boolean
    */
    public function filesExists($key){
        return isset($_FILES[$key]);
    }
    /**
    * filesData
    * Récupère une valeur en files
    * @param string $key
    * @return array<int|string>|bool
    */
    public function filesData($key){
        return $_FILES[$key];
    }

    /**
    * requestURI
    * Renvoie l'uri utiliser dans la requête du serveur
    * @return string|null
    */
    public function requestURI(){
        return $_SERVER['REQUEST_URI'];
    }
}
