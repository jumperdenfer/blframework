<?php
namespace BLFrameWork;

/** @implements \ArrayAccess<string,string|float|int|object|array<Entity|string|int|float> >  */
class Entity implements \ArrayAccess{
    /**
    * @var array<string>
    */
    protected $erreurs = [];
    /**
    * @var int|string
    */
    protected $id;
    use Traits\Hydrator;
    /**
    * @param array<string|int|float|object> $donnees
    */
    public function __construct(array $donnees = []){
        if(!empty($donnees)){
            $this->hydrate($donnees);
        }
    }

    /**
    * @return boolean
    */
    public function isNew(){
        return empty($this->id);
    }
    /**
    * @return array<string>
    */
    public function erreurs(){
        return $this->erreurs;
    }
    /**
    * @return string|int
    */
    public function id(){
        return $this->id;
    }
    /**
    * @param int|string $id
    * @return void
    */
    public function setId($id){
        $this->id = $id;
    }
    /**
    * @param string $var
    * @return int|string|float|object|array<Entity|string|int|float>|null
    */
    public function offsetGet($var){
        if(isset($this->$var) && is_callable([$this,$var])){
            return $this->$var();
        }
        else{
            return null;
        }
    }
    /**
    * @param string $var
    * @param string|float|int|object|array<Entity|string|int|float> $value
    * @return void
    */
    public function offsetSet($var,$value){
        $method = 'set'.ucFirst($var);
        if(isset($this->$var) && is_callable([$this,$method])){
            $this->$method($value);
        }
    }
    /**
    * @param string $var
    * @return boolean
    */
    public function offsetExists($var){
        return isset($this->$var) && is_callable([$this, $var]);
    }
    /**
    * @param string $var
    * @return void
    */
    public function offsetUnset($var){
        throw new \Exception('Impossible de supprimer une quelconque valeur');
    }
}
