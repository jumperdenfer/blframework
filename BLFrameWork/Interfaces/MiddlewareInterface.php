<?php
namespace BLFrameWork\Interfaces;
use BLFrameWork\Application;
interface MiddlewareInterface{
    /**
    * @param Application $app
    * @return Void
    */
    public static function run(Application $app);
}
