<?php
namespace BLFrameWork\Router;

use BLFrameWork\Router\Route;

class Router{
    /**
    * @var array<Route>
    */
    protected $route =[];
    const NO_ROUTE = 1;
    /**
    * @param Route $route
    * @return void
    */
    public function addRoute(Route $route){
        if(!in_array($route,$this->route)){
            $this->route[] = $route;
        }
    }
    /**
    * @param string|null $url
    * @return Route
    */
    public function getRoute($url){
        foreach($this->route as $route){
            $varsValue = $route->match($url);
            if( $varsValue !== false ){
                if($route->hasVars()){
                    $varsName = $route->varsNames();
                    $listVars =[];
                    foreach ($varsValue as $key => $match) {

                        if($key !== 0){
                            $listVars[$varsName[((int) $key )- 1]] = $match;
                        }
                    }
                    $route->setVars($listVars);
                }
                return $route;
            }
        }

        throw new \RuntimeException('Aucune route ne correspond à l\'URL', self::NO_ROUTE);
    }
}
