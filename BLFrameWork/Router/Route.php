<?php
namespace BLFrameWork\Router;

class Route{
    /**
    * @var string
    */
    protected $action;
    /**
    * @var string
    */
    protected $module;
    /**
    * @var string
    */
    protected $url;
    /**
    * @var array<string>
    */
    protected $varsNames = [];
    /**
    * @var array<string>
    */
    protected $vars = [];
    /**
    * @var array<string>
    */
    protected $middleware = [];

    /**
    * @param string $url
    * @param string $module
    * @param string $action
    * @param array<string> $varsNames
    * @param array<string> $middleware
    */
    public function __construct($url,$module,$action,array $varsNames,array $middleware){
        $this->setUrl($url);
        $this->setModule($module);
        $this->setAction($action);
        $this->setVarsNames($varsNames);
        $this->setMiddleware($middleware);
    }
    /**
    * @return boolean|array<string>
    */
    public function hasVars(){
        return !empty($this->varsNames);
    }
    /**
    * @param string|null $url
    * @return array<string>|false
    */
    public function match($url){
        if($url != null && preg_match('`^'.$this->url.'$`',$url,$matches)){
            return $matches;
        }
        else{
            return false;
        }
    }
    /**
    * @param string $action
    * @return void
    */
    public function setAction($action){
        if(is_string($action)){
            $this->action = $action;
        }
    }
    /**
    * @param string $module
    * @return void
    */
    public function setModule($module){
        if(is_string($module)){
            $this->module = $module;
        }
    }
    /**
    * @param string $url
    * @return void
    */
    public function setUrl($url){
        if(is_string($url)){
            $this->url = $url;
        }
    }
    /**
    * @param array<string> $varsNames
    * @return void
    */
    public function setVarsNames(array $varsNames){
        $this->varsNames = $varsNames;
    }
    /**
    * @param array<string> $vars
    * @return void
    */
    public function setVars(array $vars){
        $this->vars = $vars;
    }
    /**
    * @return string
    */
    public function action(){
        return $this->action;
    }
    /**
    * @return string
    */
    public function module(){
        return $this->module;
    }
    /**
    * @return string
    */
    public function url(){
        return $this->url;
    }
    /**
    * @return array<string>
    */
    public function vars(){
        return $this->vars;
    }
    /**
    * @return array<string>
    */
    public function varsNames(){
        return $this->varsNames;
    }
    //MiddleWare gestion
    /**
    * @param array<string> $middleware
    * @return void
    */
    public function setMiddleware(array $middleware){
        $this->middleware = $middleware;
    }
    /**
    * @return array<string>
    */
    public function middleware(){
        return $this->middleware;
    }
    /**
    * @return boolean
    */
    public function hasMiddleware(){
        return !empty($this->middleware);
    }
}
