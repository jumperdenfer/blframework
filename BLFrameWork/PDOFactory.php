<?php
namespace BLFrameWork;
class PDOFactory{
    /**
    * @param string|null $typeManager
    * @param Application $app
    * @return object
    */
    public static function get($typeManager,$app){
        $db = null;
        switch($typeManager){
            case 'PDO':
                $db = PDOFactory::getMysqlConnexion($app);
            default:
                $db = PDOFactory::getMysqlConnexion($app);
        }
        return $db;
    }
    /**
    * @param Application $app
    * @return \PDO
    */
    public static function getMysqlConnexion($app){
        $db_host = $app->config->get('db_host');
        $db_name = $app->config->get('db_name');
        $db_user = $app->config->get('db_user');
        $db_pass = $app->config->get('db_pass');
        $db = new \PDO("mysql:host={$db_host};dbname={$db_name}",$db_user, $db_pass);
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $db;
    }
}
