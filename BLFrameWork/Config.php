<?php
namespace BLFrameWork;

class Config extends ApplicationComponent{
    /**
    * @var array<string>
    */
    protected $vars = [];
    /**
    * @var array<string>
    */
    protected $globalVars = [];
    /**
    * @param string $var
    * @return string|null
    */
    public function get($var){
        if(!$this->vars){
            //Fichier de configuration Globals,
            $vendorExit = \str_replace("vendor\blagrange\blframework\BLFrameWork","",__DIR__);
            $jsonGlobal = \file_get_contents($vendorExit.'/App/config/global_app.json');
            if($jsonGlobal != false){
                $configGlobalElements = json_decode($jsonGlobal);
                //Fichier de configuration par App
                $vendorExit = \str_replace("vendor\blagrange\blframework\BLFrameWork","",__DIR__);
                $json = \file_get_contents($vendorExit.'/App/'.$this->app->name().'/config/app.json');
                if($json != false){
                    $configElements = json_decode($json);
                    foreach ($configElements as $configElement) {
                        $this->vars[$configElement->title] = $configElement->value;
                    }
                    foreach($configGlobalElements as $configGlobalElement){
                        $this->globalVars[$configGlobalElement->title] = $configGlobalElement->value;
                    }
                }
            }
        }
        //Vérifie si la variable existe dans le fichier de config spécifique à l'app.
        if(isset($this->vars[$var])){
            return $this->vars[$var];
        }
        //Si une variable n'est pas trouvé dans le fichier de config spécifié, alors on vérifie le fichier de config global_app.
        elseif(isset($this->globalVars[$var])){
            return $this->globalVars[$var];
        }
        //Si la variable n'est pas trouvé, retourne null
        return null;
    }
}
