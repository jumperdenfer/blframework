<?php
namespace BLFrameWork\Form;
/*
* Cette class permet de chargé dynamiquement des validateur depuis le framework ou l'app si précisé
*
*/
class ValidatorFactory{
    /**
    * @param string $source
    * @param string $validateur
    * @param string $message
    * @param mixed $options
    * @return Object
    */
    public static function generateValidator($source,$validateur,$message,$options = null){
        $class = '';
        $testSrc = false;
        switch($source){
            case "App":
                $class = "App\Form\Validators\\".$validateur;
                break;
            case "BLFrameWork":
                $class = "BLFrameWork\Form\Validators\\".$validateur;
                break;
            default:
                $testSrc = true;
                break;
        }
        if($testSrc == true){
            throw new \RunTimeException('Le Field doit être dans APP ou BLFrameWork');
        }
        return self::getClassValidator($class,$message,$options);
    }
    /**
    * @param string $class
    * @param string $message
    * @param mixed $options
    * @return Object
    */
    public static function getClassValidator($class,$message,$options = null){
        if(class_exists($class)){
            if($options != null){
                return new $class($message,$options);
            }
            else{
                return new $class($message);
            }
        }else{
            throw new \RunTimeException("Class non-trouvée");
        }
    }

}
