<?php

namespace BLFrameWork\Form;
use BLFrameWork\Entity;
class Form{
    /**
    * @var Entity $entity
    */
    protected $entity;
    /**
    * @var array<Field>
    */
    protected $fields = [];
    /**
    * @param Entity $entity
    */
    public function __construct(Entity $entity){
        $this->setEntity($entity);
    }
    /**
    * @param Field $field
    * @return Form
    */
    public function add(Field $field){
        $attr = $field->name();
        if(method_exists($this->entity,$attr)){
            $field->setValue($this->entity->$attr());
        }
        else{
            $attr = 'get'.ucFirst($attr);
            $field->setValue($this->entity->$attr());
        }

        $this->fields[] = $field;
        return $this;
    }
    /**
    * @return string
    */
    public function createView(){
        $view = '';

        foreach($this->fields as $field){
            $view .= $field->buildWidget();
        }

        return $view;
    }
    /**
    * @return boolean
    */
    public function isValid(){
        $valid = true;

        foreach($this->fields as $field){
            if(!$field->isValid()){
                $valid = false;
            }
        }
        return $valid;
    }

    /**
    * @return Entity
    */
    public function entity(){
        return $this->entity;
    }
    /**
    * @param Entity $entity
    * @return void
    */
    public function setEntity(Entity $entity){
        $this->entity = $entity;
    }
}
