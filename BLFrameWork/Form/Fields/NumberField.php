<?php
namespace BLFrameWork\Form\Fields;
use BLFrameWork\Form\Field;

class NumberField extends Field{
    /**
    * @var int
    */
    protected $min;
    /**
    * @var int
    */
    protected $max;
    /**
    * @var int
    */
    protected $step;
    /**
    * @return string
    */
    public function buildWidget(){
        $widget = '';
        if(!empty($this->errorMessage)){
                $widget .= $this->errorMessage.'<br>';
        }

        $widget .= "<label>{$this->label}</label><input type='number' name='{$this->name}'";
        if(!empty($this->value)){
            $widget .= " value='{$this->value}'";
        }
        if(!empty($this->min)){
            $widget .= " min='{$this->min}'";
        }
        if(!empty($this->max)){
            $widget .= " max='{$this->max}'";
        }
        if(!empty($this->step)){
            $widget .= " step={$this->step}";
        }

        return "{$widget} >";
    }
}
