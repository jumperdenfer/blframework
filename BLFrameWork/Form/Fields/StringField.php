<?php

namespace BLFrameWork\Form\Fields;
use BLFrameWork\Form\Field;
class StringField extends Field{
    /**
    * @var int
    */
    protected $maxLength;
    /**
    * @return string
    */
    public function buildWidget(){
        $widget = '';
        if(!empty($this->errorMessage)){
                $widget .= $this->errorMessage.'<br>';
        }

        $widget .= "<label>{$this->label}</label><input type='text' name='{$this->name}'";
        if(!empty($this->value)){
            $widget .= " value='{$this->value}'";
        }
        if(!empty($this->maxLength)){
            $widget .= " maxlength='{$this->maxLength}'";
        }

        return "{$widget} >";
    }
    /**
    * @param int $maxLength
    * @return void
    */
    public function setMaxLength($maxLength){
        $maxLength = (int) $maxLength;
        if($maxLength > 0){
            $this->maxLength = $maxLength;
        }else{
            throw new \RuntimeException('La longueur maximal doit être supérieur à 0');
        }
    }
}
