<?php
namespace BLFrameWork\Form\Fields;
use BLFrameWork\Form\Field;

class RadioField extends Field{
    /**
    * @var array<int|string> $radioValue
    */
    protected $radioValue;

    public function buildWidget(){
        $widget = '';
        if(!empty($this->errorMessage)){
                $widget .= $this->errorMessage.'<br>';
        }
        foreach ($this->radioValue as $value) {
            $widget .= "<label>{$value}</label><input type='radio' name='{$this->name}' value='{$value}'>";
        }
        return $widget;
    }

    /**
    * @param array<int|string> $radioArray
    * @return void
    */
    public function setRadioValue($radioArray){
        if(is_array($radioArray) && !empty($radioArray)){
            $this->radioValue = $radioArray;
        }
    }
}
