<?php
namespace BLFrameWork\Form\Fields;
use BLFrameWork\Form\Field;

class SecretField extends Field{
    public function buildWidget(){
        $widget = '';
        if(!empty($this->errorMessage)){
                $widget .= $this->errorMessage.'<br>';
        }

        $widget .= "<label>{$this->label}</label><input type='password' name='{$this->name}'";
        if(!empty($this->value)){
            $widget .= " value='{$this->value}'";
        }
        if(!empty($this->min)){
            $widget .= " min='{$this->min}'";
        }
        if(!empty($this->max)){
            $widget .= " max='{$this->max}'";
        }
        if(!empty($this->step)){
            $widget .= " step={$this->step}";
        }

        return "{$widget} >";
    }
}
