<?php
namespace BLFrameWork\Form\Fields;
use BLFrameWork\Form\Field;

class SelectField extends Field{
    /**
    * @var array<array<int|string>> $options
    **/
    protected $options;

    /**
    * @var bool
    **/
    protected $multiple;

    public function buildWidget(){
        $widget = "";
        if(!empty($this->errorMessage)){
                $widget .= $this->errorMessage.'<br>';
        }
        if($this->multiple == true){
            $widget .= "<label>{$this->label}</label><select name='{$this->name}[]' multiple>";
        }
        else{
            $widget .= "<label>{$this->label}</label><select name='{$this->name}'>";
        }
        foreach($this->options as $option){
            if($this->multiple == true && is_array($option)){
                $testValue = array_search($option['value'],array_column((array) $this->value,'value'));
                if(!empty($testValue) || $testValue === 0){
                    $widget.= "<option value='{$option['value']}' selected >{$option['label']}</option>";
                }else{
                    $widget.= "<option value='{$option['value']}'>{$option['label']}</option>";
                }
            }
            else if($this->value == $option['value']){
                $widget.= "<option value='{$option['value']}' selected>{$option['label']}</option>";
            }
            else{
                $widget.= "<option value='{$option['value']}'>{$option['label']}</option>";
            }

        }
        $widget .="</select>";
        return $widget;
    }
    /**
    * @param array<array<int|string>> $options
    * @return void
    */
    public function setOptions($options){
        if(\is_array($options) && !empty($options)){
            $this->options = $options;
        }
    }
    /**
    * @param bool $multiple
    * @return void
    */
    public function setMultiple($multiple){
        if(is_bool($multiple) && !empty($multiple)){
            $this->multiple = $multiple;
        }
        else{
            $this->multiple = false;
        }
    }
}
