<?php
namespace BLFrameWork\Form;
/**
* Super class Validator
*/
abstract class Validator{
    /**
    * @var string
    */
    protected $errorMessage = null;
    /**
    * @param string $errorMessage
    */
    public function __construct($errorMessage){
        $this->setErrorMessage($errorMessage);
    }
    /**
    * @param mixed $value
    * @return boolean
    */
    abstract public function isValid($value);

    /**
    * @param string $errorMessage
    * @return void
    */
    public function setErrorMessage($errorMessage){
        if(is_string($errorMessage)){
            $this->errorMessage = $errorMessage;
        }
    }
    /**
    * @return string
    */
    public function errorMessage(){
        return $this->errorMessage;
    }
}
