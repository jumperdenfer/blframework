<?php
namespace BLFrameWork\Form;
/*
* Permet de générais dynamiquement les class des fields afin d'éviter les nombre " use";
*/
class FieldFactory{
    /**
    * @param string $source // App or BLFrameWork
    * @param string $choosedfield // Name of the class
    * @param mixed $options //Params for the field
    * @return object|bool
    */
    public static function generateField($source,$choosedfield,$options){
        $testSrc = false;
        $class = '';
        switch($source){
            case "App" :
                $class = 'App\Form\Fields\\'.$choosedfield;
                break;
            case "BLFrameWork":
                $class = "BLFrameWork\Form\Fields\\".$choosedfield;
                break;
            default:
                $testSrc = true;
                break;
        }
        if($testSrc == true){
            throw new \RunTimeException('Le Field doit être dans APP ou BLFrameWork');
        }
        return self::getClassField($class,$options);
    }
    /**
    * @param string $class // namespace of the class
    * @param mixed $options
    * @return object
    */
    public static function getClassField($class,$options){
        if(class_exists($class)){
            return new $class($options);
        }else{
            throw new \RunTimeException("Class non-trouvée");
        }
    }
}
