<?php
namespace BLFrameWork\Form;
use BLFrameWork\Form\Form;
use BLFrameWork\Manager;
use BLFrameWork\Http\HttpRequest;

class FormHandler{
    /**
    * @var Form
    */
    protected $form;
    /**
    * @var Manager
    */
    protected $manager;
    /**
    * @var HttpRequest
    */
    protected $request;
    /**
    * @param Form $form
    * @param Manager $manager
    * @param HttpRequest $request
    */
    public function __construct(Form $form,Manager $manager, HttpRequest $request){
        $this->setForm($form);
        $this->setManager($manager);
        $this->setRequest($request);
    }
    /**
    * @param string $action //Permet de définir l'action à effectuer lors du process
    * @param boolean $validation //Si définie en true, utilise form isValid
    * @return boolean
    */
    public function process($action = 'save', $validation = true){
        //Passe par la validations de form
        if($validation == true && $this->form->isValid() && $this->request->method() == "POST"){
            $this->manager->$action($this->form->entity());
            return true;
        }
        else if($this->request->method() == "POST"){
            $this->manager->$action($this->form->entity());
            return true;
        }
        else{
            return false;
        }
    }
    /**
    * @param Form $form
    * @return void
    */
    public function setForm(Form $form){
        $this->form = $form;
    }
    /**
    * @param Manager $manager
    * @return void
    */
    public function setManager(Manager $manager){
        $this->manager = $manager;
    }
    /**
    * @param HttpRequest $request
    * @return void
    */
    public function setRequest(HttpRequest $request){
        $this->request = $request;
    }

}
