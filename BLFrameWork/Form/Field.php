<?php
namespace BLFrameWork\Form;
use BLFrameWork\Traits;
use BLFrameWork\Form\Validator;

abstract class Field{
    use Traits\Hydrator;
    /**
    * @var string
    */
    protected $errorMessage;
    /**
    * @var string
    */
    protected $label;
    /**
    * @var string
    */
    protected $name;
    /**
    * @var array<Validator>
    */
    protected $validators = [];
    /**
    * @var mixed
    */
    protected $value;
    /**
    * @param array<string|int|float> $options
    */
    public function __construct(array $options = []){
        if(!empty($options)){
            $this->hydrate($options);
        }
    }
    /**
    * @return string
    */
    abstract public function buildWidget();

    /**
    * @return boolean
    */
    public function isValid(){
        foreach($this->validators as $validator){
            if(!$validator->isValid($this->value)){
                $this->errorMessage = $validator->errorMessage();
                return false;
            }
        }
        return true;
    }

    /**
    * @return string
    */
    public function label(){
        return $this->label;
    }
    /**
    * @return string
    */
    public function name(){
        return $this->name;
    }
    /**
    * @return mixed
    */
    public function value(){
        return $this->value;
    }
    /**
    * @return array<Validator>
    */
    public function validator(){
        return $this->validators;
    }
    /**
    * @param string $label
    * @return void
    */
    public function setLabel($label){
        if(is_string($label)){
            $this->label = htmlspecialchars($label,ENT_QUOTES);
        }
    }
    /**
    * @param string $name
    * @return void
    */
    public function setName($name){
        if(is_string($name)){
            $this->name = htmlspecialchars($name,ENT_QUOTES);
        }
    }
    /**
    * @param array<Validator> $validators
    * @return void
    */
    public function setValidators(array $validators){
        foreach($validators as $validator){
            if($validator instanceof Validator && !in_array($validator, $this->validators)){
                $this->validators[] = $validator;
            }
        }
    }
    /**
    * @param mixed $values
    * @return void
    */
    public function setValue($values){
        if(is_string($values)){
            $this->value = htmlspecialchars($values,ENT_QUOTES);
        }
        if(is_array($values)){
            foreach($values as $val){
                $val['value'] = htmlspecialchars((string) $val['value'],ENT_QUOTES);
            }
            $this->value = $values;
        }
    }
}
