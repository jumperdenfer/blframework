<?php
/**
* Class abstraite FormBuilder, permettant la création des formulaire de façon simplifier
*/
namespace  BLFrameWork\Form;

use BLFrameWork\Entity;
use BLFrameWork\Form\Form;

abstract class FormBuilder{
    /**
    * @var Form
    */
    protected $form;
    /**
    * @param Entity $entity
    */
    public function __construct(Entity $entity){
        $this->setForm(new Form($entity));
    }
    /**
    * @return void
    */
    abstract public function build();
    /**
    * @param Form $form
    * @return void
    */
    public function setForm(Form $form){
        $this->form = $form;
    }
    /**
    * @return Form
    */
    public function form(){
        return $this->form;
    }
}
