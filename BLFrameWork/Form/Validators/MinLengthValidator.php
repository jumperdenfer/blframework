<?php

namespace BLFrameWork\Form\Validators;
use BLFrameWork\Form\Validator;

class MinLengthValidator extends Validator{
    /**
    * @var int $minLength
    */
    protected $minLength;
    /**
    * @param string $errorMessage
    * @param int $minLength
    */
    public function __construct($errorMessage,$minLength){
        parent::__construct($errorMessage);
        $this->setMinLength($minLength);
    }
    /**
    * @param string $value
    * @return boolean
    */
    public function isValid($value){
        return strlen($value) >= $this->minLength;
    }
    /**
    * @param int $minLength
    * @return void
    */
    public function setMinLength($minLength){
        $minLength = (int) $minLength;
        if($minLength > 0){
            $this->minLength = $minLength;
        }
        else{
            throw new \RuntimeException("La longueur minimum doit être un nombre supérieur à 0");
        }
    }
}
