<?php
namespace BLFrameWork\Form\Validators;
use BLFrameWork\Form\Validator;

class MaxLengthValidator extends Validator{
    /**
    * @var int
    */
    protected $maxLength;
    /**
    * @param string $errorMessage
    * @param int $maxLength
    */
    public function __construct($errorMessage,$maxLength){
        parent::__construct($errorMessage);
        $this->setMaxLength($maxLength);
    }
    /**
    * @param string $value
    * @return boolean
    */
    public function isValid($value){
        return strlen($value) <= $this->maxLength;
    }
    /**
    * @param int $maxLength
    * @return void 
    */
    public function setMaxLength($maxLength){
        $maxLength = (int) $maxLength;
        if($maxLength > 0){
            $this->maxLength = $maxLength;
        }
        else{
            throw new \RuntimeException("La longueur maximale doit être un nombre supérieur à 0");
        }
    }
}
