<?php
namespace BLFrameWork\Form\Validators;
use BLFrameWork\Form\Validator;

class FloatValidator extends Validator{
    /**
    * @param string $errorMessage
    */
    public function __construct($errorMessage){
        parent::__construct($errorMessage);
    }
    /**
    * @param float $value
    * @return boolean
    */
    public function isValid($value){
        return is_float($value);
    }
}
