<?php
namespace  BLFrameWork\Form\Validators;
use BLFrameWork\Form\Validator;

class NotNullValidator extends Validator{
    /**
    * @param mixed $value
    * @return boolean
    */
    public function isValid($value){
        return !empty($value);
    }
}
