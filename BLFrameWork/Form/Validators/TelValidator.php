<?php
namespace BLFrameWork\Form\Validators;
use BLFrameWork\Form\Validator;

class TelValidator extends Validator{

    /**
    * @param string $errorMessage
    */
    public function __construct($errorMessage){
        parent::__construct($errorMessage);
    }
    /**
    * @param int $value;
    * @return boolean
    */
    public function isValid($value){
        $phoneFiltered = filter_var($value,FILTER_SANITIZE_NUMBER_INT);
        if($phoneFiltered != false){
            $phoneFiltered = str_replace('-', "", $phoneFiltered);
            if(strlen($phoneFiltered) < 10 || strlen($phoneFiltered) > 14){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            return false;
        }
    }
}
