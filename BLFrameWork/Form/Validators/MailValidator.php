<?php
namespace BLFrameWork\Form\Validators;
use BLFrameWork\Form\Validator;

class MailValidator extends Validator{
    /**
    * @param string $errorMessage
    */
    public function __construct($errorMessage){
        parent::__construct($errorMessage);
    }
    /**
    * @param string $value
    * @return boolean
    */
    public function isValid($value){
        return filter_var($value,FILTER_VALIDATE_EMAIL) ? true : false;
    }
}
