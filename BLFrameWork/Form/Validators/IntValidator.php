<?php
namespace BLFrameWork\Form\Validators;
use BLFrameWork\Form\Validator;

class IntValidator extends Validator{
    /**
    * @param string $errorMessage
    */
    public function __construct($errorMessage){
        parent::__construct($errorMessage);
    }
    /**
    * @param int $value
    * @return boolean
    */
    public function isValid($value){
        return is_int($value);
    }
}
