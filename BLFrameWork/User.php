<?php
namespace BLFrameWork;
session_start();


class User{
    /**
    * @param string $attr
    * @return string|int|array<int|string|object>|object
    */
    public function getAttribute(string $attr){
        return $_SESSION[$attr] ?? null;
    }
    /**
    * @return string|int|float
    */
    public function getFlash(){
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        return $flash;
    }
    /**
    * @return boolean
    */
    public function hasFlash() {
        return  isset($_SESSION['flash']);
    }
    /**
    * @return boolean
    */
    public function isAuthenticated(){
        return isset($_SESSION['auth'])  && $_SESSION['auth'] == true;
    }
    /**
    * @param string $attr
    * @param string|int|float|array<string|int|float>|object $value
    * @return void
    */
    public function setAttribute($attr,$value){
        $_SESSION[$attr] = $value;
    }
    /**
    * @param boolean $authenticated
    * @return void
    */
    public function setAuthenticated($authenticated = true){
        if(!is_bool($authenticated)){
            throw new \InvalidArgumentException('La valeur spécifié à la méthode User::setAuthenticated() doit être un boolean');
        }
        $_SESSION['auth'] = $authenticated;
    }
    /**
    * @param string|array<string|int|float|object>|int|float|object $value
    * @return void
    */
    public function setFlash($value){
        $_SESSION['flash'] = $value;
    }
}
