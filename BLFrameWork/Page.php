<?php
namespace BLFrameWork;

use BLFrameWork\ApplicationComponent;
class Page extends ApplicationComponent{
    /**
    * @var string
    */
    protected $contentFiles;
    /**
    * @var array<string>
    */
    protected $vars = [];
    /**
    * @var string
    */
    protected $layout = "Layout";

    /**
    * @param string $var
    * @param string $value
    * @return void
    */
    public function addVar($var,$value){
        if(!is_string($var) || is_numeric($var) || empty($var)){
            throw new \InvalidArgumentException('Le nom de la variable doit être une chaine de charactères non nulle');
        }
        $this->vars[$var] = $value;
    }
    /**
    * @param string $var
    * @return string
    */
    public function existVar($var){
        return $this->vars[$var];
    }
    /**
    * @param string $layout
    * @return void
    */
    public function setLayout($layout){
        $this->layout = $layout;
    }
    /**
    * @return string
    */
    public function getLayoutName(){
        return $this->layout;
    }
    /**
    * @return string|boolean
    */
    public function getGeneratedPage(){
        if(!file_exists($this->contentFiles)){
            throw new \RuntimeException('La vue spécifié n\'existe pas');
        }
        $user = $this->app->user;

        extract($this->vars);
        ob_start();
            require $this->contentFiles;
        $content =  ob_get_clean();

        ob_start();
            $vendorExit = \str_replace("vendor\blagrange\blframework\BLFrameWork","",__DIR__);
            if(\file_exists($vendorExit.'/Resources/Templates/'.$this->layout.'.php')){
                require $vendorExit.'/Resources/Templates/'.$this->layout.'.php';
            }else{
                //Nécessaire car le throw ne termine pas l'opérations
                ob_get_clean();
                throw new \RuntimeException('Aucun templates selectionner');
            }
        return ob_get_clean();
    }
    /**
    * @param string $contentFiles
    * @return void
    */
    public function setContentFile($contentFiles){
        if(!is_string($contentFiles) || empty($contentFiles)){
            throw new \InvalidArgumentException('La vue spécifié est invalide');
        }
        $this->contentFiles = $contentFiles;
    }
}
