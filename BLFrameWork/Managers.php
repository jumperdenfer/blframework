<?php
namespace BLFrameWork;

class Managers{
    /**
    * @var string|null
    */
    protected $api = null;
    /**
    * @var object
    */
    protected $dao = null;
    /**
    * @var string
    */
    protected $appName;
    /**
    * @var array<Manager>
    */
    protected $managers = [];
    /**
    * @param Application $app
    */
    public function __construct($app){
        $typeManager = $app->config->get('PDOManager');
        $this->api = $typeManager;
        $this->dao = PDOFactory::get($typeManager,$app);
        $this->appName = $app->name();
    }
    /**
    * @param string $module
    * @return Manager
    */
    public function getManagerOf($module){
        if(!is_string($module) || empty($module)){
            throw new \InvalidArgumentException('Le module spécifié est invalide');
        }
        if(!isset($this->managers[$module])){
            //Si un manager est spécifique à un module ( utile dans certain cas)
            if(class_exists('App\\'.$this->appName.'\\Model\\'.$module.'Manager'.$this->api)){
                $manager = 'App\\'.$this->appName.'\\Model\\'.$module.'Manager'.$this->api;
            }
            //Sinon regarde dans les managers global
            else{
                $manager = 'App\\Model\\'.$module.'\\'.$module.'Manager'.$this->api;
            }
            $this->managers[$module] = new $manager($this->dao);
        }
        return $this->managers[$module];
    }
}
