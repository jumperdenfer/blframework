<?php
namespace BLFrameWork\Tools;

/**
* Tools for helping Developpers with some static function to hash their value
*/
class PasswordGestion{
    /**
    * Generate a password and return the original and the hashed in array
    * @param string $stringToHash
    * @param int|null $salt
    * @param int|null $cost
    * @return array<string,string|false>
    */
    public static function generateDefaultHash($stringToHash,$salt = null ,$cost = null){
        $options = [];
        if(is_int($cost) && !empty($cost)){
            $options += ['cost' => $cost];
        }
        if(is_int($salt) && !empty($salt)){
            $options += ['salt' => $salt];
        }

        $passwordHashed = \password_hash($stringToHash,PASSWORD_DEFAULT,$options);

        return [
            'origin'  => $stringToHash,
            'hashed' => $passwordHashed
        ];
    }
    /**
    * Verify a password
    * @param string $origin
    * @param string $hash
    * @return bool
    */
    public static function verifyPassword($origin,$hash){
        return \password_verify($origin,$hash);
    }

    /**
    * Create a string of X length for the user, with some special chars.
    * @param int $length
    * @return array<string,string|false>
    */
    public static function generateRandomPassword($length = 20){

        $length = 20;
        //Détermine quel charactères doivent être utiliser dans le token
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-()_^îôâ^zê`ñõã';
        //Correspond au nombre de charactères utiliser
        $charactersLength = strlen($characters);
        //Définie la variable qui vas contenir temporairement le token
        $randomString = '';
        //Boucle basé sur lenght
        for ($i = 0; $i < $length; $i++) {
            //Ajoute les charactère dans randomString
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return self::generateDefaultHash($randomString);
    }
}
